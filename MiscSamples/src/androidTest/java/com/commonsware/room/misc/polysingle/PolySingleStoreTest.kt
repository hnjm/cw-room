/*
  Copyright (c) 2019-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.misc.polysingle

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.commonsware.room.misc.MiscDatabase
import com.natpryce.hamkrest.anyOf
import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import com.natpryce.hamkrest.hasSize
import com.natpryce.hamkrest.isEmpty
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class PolySingleStoreTest {
  private val db = Room.inMemoryDatabaseBuilder(
    InstrumentationRegistry.getInstrumentation().targetContext,
    MiscDatabase::class.java
  )
    .build()
  private val underTest = db.polySingleStore()

  @Test
  fun comments() {
    assertThat(underTest.allComments(), isEmpty)

    val firstComment = Comment(1, "This is a comment")
    val secondComment = Comment(2, "This is another comment")

    underTest.insert(firstComment, secondComment)

    val allComments = underTest.allComments()

    assertThat(allComments, hasSize(equalTo(2)))
    assertThat(
      allComments[0].title,
      anyOf(equalTo(firstComment.title), equalTo(secondComment.title))
    )
    assertThat(
      allComments[1].title,
      anyOf(equalTo(firstComment.title), equalTo(secondComment.title))
    )

    val allNotes = underTest.allNotes()

    assertThat(allNotes, hasSize(equalTo(2)))
    assertThat(
      allNotes[0].title,
      anyOf(equalTo(firstComment.title), equalTo(secondComment.title))
    )
    assertThat(
      allNotes[1].title,
      anyOf(equalTo(firstComment.title), equalTo(secondComment.title))
    )
  }

  @Test
  fun links() {
    assertThat(underTest.allLinks(), isEmpty)

    val firstLink = Link(1, "CommonsWare", "https://commonsware.com")
    val secondLink = Link(
      2,
      "Room Release Notes",
      "https://developer.android.com/jetpack/androidx/releases/room"
    )

    underTest.insert(firstLink, secondLink)

    val allLinks = underTest.allLinks()

    assertThat(allLinks, hasSize(equalTo(2)))
    assertThat(
      allLinks[0].title,
      anyOf(equalTo(firstLink.title), equalTo(secondLink.title))
    )
    assertThat(
      allLinks[1].title,
      anyOf(equalTo(firstLink.title), equalTo(secondLink.title))
    )

    val allNotes = underTest.allNotes()

    assertThat(allNotes, hasSize(equalTo(2)))
    assertThat(
      allNotes[0].title,
      anyOf(equalTo(firstLink.title), equalTo(secondLink.title))
    )
    assertThat(
      allNotes[1].title,
      anyOf(equalTo(firstLink.title), equalTo(secondLink.title))
    )
  }

  @Test
  fun notes() {
    assertThat(underTest.allNotes(), isEmpty)

    val firstComment = Comment(1, "This is a comment")
    val secondComment = Comment(2, "This is another comment")
    val firstLink = Link(3, "CommonsWare", "https://commonsware.com")
    val secondLink = Link(
      4,
      "Room Release Notes",
      "https://developer.android.com/jetpack/androidx/releases/room"
    )

    underTest.insert(firstComment, secondComment, firstLink, secondLink)

    val allNotes = underTest.allNotes()

    assertThat(allNotes, hasSize(equalTo(4)))
    assertThat(
      allNotes[0].title,
      anyOf(
        equalTo(firstComment.title),
        equalTo(secondComment.title),
        equalTo(firstLink.title),
        equalTo(secondLink.title)
      )
    )
    assertThat(
      allNotes[1].title,
      anyOf(
        equalTo(firstComment.title),
        equalTo(secondComment.title),
        equalTo(firstLink.title),
        equalTo(secondLink.title)
      )
    )
    assertThat(
      allNotes[2].title,
      anyOf(
        equalTo(firstComment.title),
        equalTo(secondComment.title),
        equalTo(firstLink.title),
        equalTo(secondLink.title)
      )
    )
    assertThat(
      allNotes[3].title,
      anyOf(
        equalTo(firstComment.title),
        equalTo(secondComment.title),
        equalTo(firstLink.title),
        equalTo(secondLink.title)
      )
    )
  }
}