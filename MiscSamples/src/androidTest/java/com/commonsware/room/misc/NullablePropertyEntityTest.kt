package com.commonsware.room.misc

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import com.natpryce.hamkrest.isEmpty

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import java.util.*

@RunWith(AndroidJUnit4::class)
class NullablePropertyEntityTest {
  private val db = Room.inMemoryDatabaseBuilder(
    InstrumentationRegistry.getInstrumentation().targetContext,
    MiscDatabase::class.java
  )
    .build()
  private val underTest = db.nullableProperty()

  @Test
  fun propertyIgnored() {
    assertThat(underTest.loadAll(), isEmpty)

    val original = NullablePropertyEntity(
      id = UUID.randomUUID().toString(),
      title = "This is a title. No, really, it is!"
    )

    underTest.insert(original)

    val retrieved = underTest.findByPrimaryKey(original.id)

    assertThat(retrieved.id, equalTo(original.id))
    assertThat(retrieved.title, equalTo(original.title))
    assertThat(retrieved.text, equalTo(null as String?))
  }
}
