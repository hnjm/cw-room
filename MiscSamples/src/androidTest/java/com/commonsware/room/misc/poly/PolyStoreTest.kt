/*
  Copyright (c) 2019-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.misc.poly

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.commonsware.room.misc.MiscDatabase
import com.natpryce.hamkrest.*
import com.natpryce.hamkrest.assertion.assertThat
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class PolyStoreTest {
  private val db = Room.inMemoryDatabaseBuilder(
    InstrumentationRegistry.getInstrumentation().targetContext,
    MiscDatabase::class.java
  )
    .build()
  private val underTest = db.polyStore()

  @Test
  fun comments() {
    assertThat(underTest.allComments(), isEmpty)

    val firstComment = CommentEntity(1, "This is a comment")
    val secondComment = CommentEntity(2, "This is another comment")

    underTest.insert(firstComment, secondComment)

    assertThat(
      underTest.allComments(),
      allOf(
        hasSize(equalTo(2)),
        hasElement(firstComment),
        hasElement(secondComment)
      )
    )

    assertThat(
      underTest.allNotes(),
      allOf(
        hasSize(equalTo(2)),
        hasElement(firstComment as Note),
        hasElement(secondComment as Note)
      )
    )
  }

  @Test
  fun links() {
    assertThat(underTest.allLinks(), isEmpty)

    val firstLink = LinkEntity(1, "CommonsWare", "https://commonsware.com")
    val secondLink = LinkEntity(
      2,
      "Room Release Notes",
      "https://developer.android.com/jetpack/androidx/releases/room"
    )

    underTest.insert(firstLink, secondLink)

    assertThat(
      underTest.allLinks(),
      allOf(
        hasSize(equalTo(2)),
        hasElement(firstLink),
        hasElement(secondLink)
      )
    )

    assertThat(
      underTest.allNotes(),
      allOf(
        hasSize(equalTo(2)),
        hasElement(firstLink as Note),
        hasElement(secondLink as Note)
      )
    )
  }

  @Test
  fun notes() {
    assertThat(underTest.allNotes(), isEmpty)

    val firstComment = CommentEntity(1, "This is a comment")
    val secondComment = CommentEntity(2, "This is another comment")
    val firstLink = LinkEntity(1, "CommonsWare", "https://commonsware.com")
    val secondLink = LinkEntity(
      2,
      "Room Release Notes",
      "https://developer.android.com/jetpack/androidx/releases/room"
    )

    underTest.insert(firstComment, secondComment, firstLink, secondLink)

    assertThat(
      underTest.allNotes(), allOf(
        hasSize(equalTo(4)),
        hasElement(firstLink as Note),
        hasElement(secondLink as Note),
        hasElement(firstComment as Note),
        hasElement(secondComment as Note)
      )
    )
  }
}