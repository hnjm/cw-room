/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.misc

import android.database.sqlite.SQLiteConstraintException
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.natpryce.hamkrest.*
import com.natpryce.hamkrest.assertion.assertThat

import org.junit.Test
import org.junit.runner.RunWith

import java.util.*

private const val TEST_TITLE = "A Tale of Two Entities"

@RunWith(AndroidJUnit4::class)
class UniqueIndexEntityTest {
  private val db = Room.inMemoryDatabaseBuilder(
    InstrumentationRegistry.getInstrumentation().targetContext,
    MiscDatabase::class.java
  )
    .build()
  private val underTest = db.uniqueIndex()

  @Test
  fun singleInsert() {
    assertThat(underTest.loadAll(), isEmpty)

    val firstEntity = UniqueIndexEntity(
      id = UUID.randomUUID().toString(),
      title = TEST_TITLE,
      text = "This entity will get inserted successfully")

    underTest.insert(firstEntity)

    assertThat(
      underTest.loadAll(),
      allOf(hasSize(equalTo(1)), hasElement(firstEntity))
    )
  }

  @Test(expected = SQLiteConstraintException::class)
  fun duplicateFailure() {
    singleInsert()

    val secondEntity = UniqueIndexEntity(
      id = UUID.randomUUID().toString(),
      title = TEST_TITLE,
      text = "This entity is doomed")

    underTest.insert(secondEntity)
  }
}
