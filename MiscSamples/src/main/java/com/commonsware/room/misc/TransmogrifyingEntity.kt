/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.misc

import android.location.Location
import android.util.JsonReader
import android.util.JsonWriter
import androidx.room.*
import org.threeten.bp.Instant
import java.io.StringReader
import java.io.StringWriter

@Entity(tableName = "transmogrified")
@TypeConverters(TypeTransmogrifier::class)
data class TransmogrifyingEntity(
  @PrimaryKey(autoGenerate = true)
  val id: Long = 0,
  val creationTime: Instant = Instant.now(),
  val location: Location = Location(null as String?).apply {
    latitude = 0.0
    longitude = 0.0
  },
  val tags: Set<String> = setOf()
) {
  @Dao
  interface Store {
    @Query("SELECT * FROM transmogrified")
    fun loadAll(): List<TransmogrifyingEntity>

    @Insert
    fun insert(entity: TransmogrifyingEntity)
  }
}

class TypeTransmogrifier {
  @TypeConverter
  fun instantToLong(timestamp: Instant?) = timestamp?.toEpochMilli()

  @TypeConverter
  fun longToInstant(timestamp: Long?) =
    timestamp?.let { Instant.ofEpochMilli(it) }

  @TypeConverter
  fun locationToString(location: Location?) =
    location?.let { "${it.latitude};${it.longitude}" }

  @TypeConverter
  fun stringToLocation(location: String?) = location?.let {
    val pieces = location.split(';')

    if (pieces.size == 2) {
      try {
        Location(null as String?).apply {
          latitude = pieces[0].toDouble()
          longitude = pieces[1].toDouble()
        }
      } catch (e: Exception) {
        null
      }
    } else {
      null
    }
  }

  @TypeConverter
  fun stringSetToString(list: Set<String>?) = list?.let {
    val sw = StringWriter()
    val json = JsonWriter(sw)

    json.beginArray()
    list.forEach { json.value(it) }
    json.endArray()
    json.close()

    sw.buffer.toString()
  }

  @TypeConverter
  fun stringToStringSet(stringified: String?) = stringified?.let {
    val json = JsonReader(StringReader(it))
    val result = mutableSetOf<String>()

    json.beginArray()

    while (json.hasNext()) {
      result += json.nextString()
    }

    json.endArray()

    result.toSet()
  }
}