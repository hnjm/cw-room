/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.misc.onetomany

import androidx.room.*

data class CategoryAndBooks(
  @Embedded
  val category: Category,
  @Relation(
    parentColumn = "shortCode",
    entityColumn = "categoryShortCode"
  )
  val books: List<Book>
)

@Dao
interface Bookstore {
  @Insert
  suspend fun save(category: Category)

  @Insert
  suspend fun save(vararg books: Book)

  @Transaction
  @Query("SELECT * FROM categories")
  suspend fun loadAll(): List<CategoryAndBooks>

  @Transaction
  @Query("SELECT * FROM categories WHERE shortCode = :shortCode")
  suspend fun loadByShortCode(shortCode: String): CategoryAndBooks
}